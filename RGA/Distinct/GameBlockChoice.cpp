#include "GameBlockChoice.h"

namespace RGA {
    GameBlockChoice::GameBlockChoice(GameBlock *block, const std::string &text, const std::optional<std::function<bool(const std::map<std::string, bool> &boolVars, const std::map<std::string, short int> &intVars)>> &condition) {
        this->block = block;
        this->text = text;
        this->condition = condition;
    }

    std::string GameBlockChoice::getText() const {
        return text;
    }

    const std::string &GameBlockChoice::viewText() const {
        return text;
    }

    bool GameBlockChoice::isValid(const std::map<std::string, bool> &boolVars, const std::map<std::string, short int> &intVars) const {
        return !condition.has_value() ? true : condition.value()(boolVars, intVars);
    }

    GameBlock *GameBlockChoice::getBlock() const {
        return block;
    }
}